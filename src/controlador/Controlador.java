package controlador;
import modelo.Bomba;
import modelo.Gasolina;
import vista.dlgBomba;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.WindowConstants;

public class Controlador implements ActionListener{
    
    private Bomba bomba;
    private Gasolina gas;
    private dlgBomba vista;

    public Controlador(Bomba bomba, Gasolina gas, dlgBomba vista){
        
        this.bomba = bomba;
        this.gas = gas;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnIniciarBomba.addActionListener(this);
        vista.btnRegistrarVenta.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
    }
    
    private void iniciarVista(){
       vista.setTitle("Despachador de gasolina");
       vista.setSize(830,690);
       vista.setLocationRelativeTo(null);
       vista.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
       vista.setVisible(true);
       vista.dispose();
       System.exit(0);
   }
    
    private float maximaVenta(){
        float total = gas.getPrecio()*(bomba.getCapacidadMaxima()-bomba.getAcumuladorLitros());
        return total;
    }
    
   @Override
   public void actionPerformed(ActionEvent e) {
       
       if(e.getSource() == vista.btnCerrar){
           int result = JOptionPane.showConfirmDialog(vista, "¿Seguro que deseas salir?", "Advertencia", JOptionPane.YES_NO_OPTION);
           if (result == JOptionPane.YES_OPTION)
               System.exit(0);
           else if (result == JOptionPane.NO_OPTION)
               vista.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
       }
       
       if(e.getSource() == vista.btnCancelar){
           vista.txtNumeroBomba.setText("");
           vista.txtPrecioGasolina.setText("");
           vista.txtNumeroBomba.setEnabled(false);
           vista.txtContadorVentas.setText("0");
           vista.txtContadorVentas.setDisabledTextColor(Color.gray);
           vista.comboTipoGasolina.setEnabled(false);
           vista.comboTipoGasolina.setSelectedIndex(0);
           vista.txtPrecioGasolina.setEnabled(false);
           vista.lbCosto.setText("");
           vista.lbTotalDeVentas.setText("");
           vista.txtCantidadVender.setEnabled(false);
           vista.txtCantidadVender.setText("");
           vista.txtCantidadVender.setBorder(BorderFactory.createLineBorder(Color.gray));
       }
       
       if(e.getSource() == vista.btnNuevo){

           vista.txtNumeroBomba.setEnabled(true);
           vista.txtContadorVentas.setText("0");
           vista.txtContadorVentas.setDisabledTextColor(Color.black);
           vista.comboTipoGasolina.setEnabled(true);
           vista.txtPrecioGasolina.setEnabled(true);
           vista.slideCapacidad.setValue(200);
       }
       
       if(e.getSource() == vista.btnLimpiar){
           vista.txtNumeroBomba.setText("");
           vista.comboTipoGasolina.setSelectedIndex(0);
           vista.txtPrecioGasolina.setText("");
           vista.txtContadorVentas.setText("");
           vista.lbCosto.setText("");
           vista.lbTotalDeVentas.setText("");
           vista.txtCantidadVender.setText("");
           vista.txtCantidadVender.setEnabled(false);
           vista.txtCantidadVender.setBorder(BorderFactory.createLineBorder(Color.gray));
       }
       
       if(e.getSource() == vista.btnRegistrarVenta){
           float vendido;
           try {
                float cantidadAVender = Float.parseFloat(vista.txtCantidadVender.getText());
                float cantidadAVenderLitros = cantidadAVender / gas.getPrecio();
                if(cantidadAVenderLitros > bomba.inventarioGasolina()){
                    JOptionPane.showMessageDialog(vista, "No es posible vender esa cantidad, cantidad máxima a vender: " + maximaVenta() );
                } else {
                    vendido = bomba.venderGasolina(cantidadAVender);
                    vista.lbCosto.setText(Float.toString(vendido));
                    vista.lbTotalDeVentas.setText(Float.toString(bomba.ventasTotales()));
                    vista.txtContadorVentas.setText(Float.toString(bomba.getAcumuladorLitros()));
                    vista.slideCapacidad.setValue((int)bomba.inventarioGasolina());
                    vista.lbVentaMaxima.setText(Float.toString(maximaVenta()));
                    JOptionPane.showMessageDialog(vista, "Venta registrada exitosamente" );
                }
           } catch(NumberFormatException exc){
               JOptionPane.showMessageDialog(vista, "Error en la conversion de numero " + exc.getMessage());
           } finally {
               if(vista.slideCapacidad.getValue() == 0){
                   vista.btnRegistrarVenta.setEnabled(false);
                   vista.txtCantidadVender.setEnabled(false);
                   vista.txtCantidadVender.setBorder(BorderFactory.createLineBorder(Color.gray));
                   JOptionPane.showMessageDialog(vista, "No es posible realizar más ventas, la bomba esta vacía");
               }
           }  
       }
       
       if(e.getSource() == vista.btnIniciarBomba){
           try{
                float contador =0;
                gas.setIdGasolina(vista.comboTipoGasolina.getSelectedIndex());
                gas.setTipo(String.valueOf(vista.comboTipoGasolina.getSelectedItem()));
                gas.setPrecio(Float.parseFloat(vista.txtPrecioGasolina.getText()));
                bomba.iniciarBomba(Integer.parseInt(vista.txtNumeroBomba.getText()), gas);
                if(gas.getPrecio()!= 0 && gas.getTipo() != null && gas.getIdGasolina() != 0 && bomba.getCapacidadMaxima() > 0 && bomba.getNumeroBomba() !=0){
                    vista.txtCantidadVender.setEnabled(true);
                    vista.txtCantidadVender.setBorder(BorderFactory.createLineBorder(Color.green));
                    JOptionPane.showMessageDialog(vista, "Bomba iniciada correctamente\n Cantidad maxima a vender: " + maximaVenta());
                    vista.lbVentaMaxima.setText(Float.toString(maximaVenta()));
                    vista.btnIniciarBomba.setEnabled(false);
                }
           } catch(NumberFormatException exc){
               JOptionPane.showMessageDialog(vista, "Error en la conversion de numeros " + exc.getMessage());
           }
       }
   }
    
    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        Gasolina gas = new Gasolina();
        
        dlgBomba vista = new dlgBomba(new JFrame(),true);
        Controlador control = new Controlador(bomba, gas, vista);
        control.iniciarVista();
    }

}
