package modelo;

public class Bomba {
    int numBomba;
    Gasolina gasolina;
    float capacidad;
    float contLitros;
    
    public Bomba(){
        
    }
    public Bomba(int numeroBomba, Gasolina gasolina, float capacidad, float contlitros){
        this.numBomba = numeroBomba;
        this.gasolina = gasolina;
        this.capacidad = capacidad;
        this.contLitros = contlitros;
    }
    
    public Bomba(Bomba bomba){
        this.numBomba = bomba.numBomba;
        this.gasolina = bomba.gasolina;
        this.capacidad = bomba.capacidad;
        this.contLitros = bomba.contLitros;
    }
    
    public void iniciarBomba(int numeroBomba, Gasolina gasolina){
        this.numBomba = numeroBomba;
        this.gasolina = gasolina;
        this.capacidad = 200;
        this.contLitros = 0;
    }
    
    public float inventarioGasolina(){
        return getCapacidadMaxima() - getAcumuladorLitros();
    }
    
    public float venderGasolina(float cantidad){
        float vendido=0;
        float litros;
        litros = (cantidad/gasolina.precio);
        if (litros <= capacidad){
            contLitros += litros;
            vendido = litros*gasolina.precio;
            return vendido;
        }
        else
            return 0;
    }
    public float ventasTotales(){
        float ventasTotales;
        ventasTotales = contLitros * gasolina.precio;
        return ventasTotales;
    }
    
    public int getNumeroBomba(){
        return numBomba;
    }
    
    public void setNumeroBomba(int numeroBomba){
        this.numBomba = numeroBomba;
    }
    
    public Gasolina getGasolina(){
        return gasolina;
    }
    public void setGasolina(Gasolina gasolina){
        this.gasolina = gasolina;
    }
    
    public float getCapacidadMaxima(){
        return capacidad;
    }
    
    public float getAcumuladorLitros(){
        return contLitros;
    }
}
