package modelo;

public class Gasolina {
    int idGasolina;
    String tipo;
    float precio;
    
    public Gasolina(){
        
    }
    public Gasolina(int idGasolina, String tipo, float precio){
        this.idGasolina = idGasolina;
        this.precio =  precio;
        this.tipo = tipo;
    }
    
    public Gasolina (Gasolina gasolina){
        this.idGasolina = gasolina.idGasolina;
        this.precio = gasolina.precio;
        this.tipo = gasolina.tipo;
    }
    
    public int getIdGasolina(){
        return idGasolina;
    }
    public void  setIdGasolina(int id){
        idGasolina=id;
    }
    public String getTipo(){
        return tipo;
    }
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    public float getPrecio(){
        return precio;
    }
    public void setPrecio(float precio){
        this.precio = precio;
    }
}
